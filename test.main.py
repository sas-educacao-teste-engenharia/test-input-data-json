import json
import random
import requests
import os

#! Only need this if you're running this code locally.
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())
#! Only need this if you're running this code locally.

#! Environ
SECRET_KEY_VALUE = os.environ.get('SECRET_KEY_VALUE')

#! Global variables
url = "http://localhost:8080/"


def test_request(payload):
    """Function request in local host.

    Args:
        payload (dict): Dictionary with data provided.   

    Returns:
        int: Status code.
    """
    headers = {
        'Authorization': f'Bearer {SECRET_KEY_VALUE}',
        'Content-Type': 'application/json'
    }
    response = requests.request(
        "POST",
        url,
        headers=headers,
        data=json.dumps(payload)
    )

    print(json.loads(response.text.encode('utf8')))

    return response.status_code


# Read data provided.
with open('test-input-data-json/data/data_question.json', 'r') as file:
    data = json.loads(file.read())

for idx, question in enumerate(data[:10]):
    print(question['id'])

    # Drop key
    if idx % 2 == 0:
        question.pop(random.choice(list(question.keys())), None)

    # Remove data
    if idx % 3 == 0:
        question[random.choice(list(question.keys()))] = None

    # Change dict to array and duplicate
    if idx % 5 == 0:
        question = [question]*idx

    print(question)

    # Run test
    print(test_request(question))
