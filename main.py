from flask import jsonify
import jwt
import os
import json
from uuid import uuid4
from utils_gcs import gcs_upload_json
from utils_pubsub import publisher_pubsub

# #! Only need this if you're running this code locally.
# from dotenv import load_dotenv, find_dotenv
# load_dotenv(find_dotenv())
# #! Only need this if you're running this code locally.

#! Environ
SECRET_KEY = os.environ.get('SECRET_KEY')
GCP_PROJECT_ID = os.environ.get('GCP_PROJECT_ID')
TOPIC_NAME = os.environ.get('TOPIC_NAME')

#! Global variables
bucket_name = "test_raw_zone"

# Sample data for key and type analysis
example = {
    "id": 0,
    "statement": "",
    "options": [],
    "answer_key": 0,
    "difficulty": "",
    "grade": 0,
    "lecture": 0,
    "skill": {},
    "properties": []
}


def verification_data(data_dict):
    data_type = type(data_dict)

    if data_type is dict:
        data_dict = [data_dict]

    data_verificated = {'error': False, 'errors': []}
    id_list = []

    for idx, data_input in enumerate(data_dict):

        data_erro_type = [
            {key: {
                'expected_type': str(type(example[key])),
                'sent_type':str(type(data_input.get(key)))
            }}
            for key in example
            if type(data_input.get(key)) != type(example[key])
        ]

        data_missing = [
            key
            for key in example
            if data_input.get(key) == None
        ]

        if (data_erro_type != []) or (data_missing != []):
            print('Erro missing key or erro type')
            data_verificated['error'] = True
            data_verificated['errors'].append({
                'id': data_input.get('id'),
                'position': idx,
                'missing_keys': data_missing,
                'error_key_type': data_erro_type,
                'status': 'Document not save'
            }
            )
        elif (data_input.get('id') != None) and (data_input.get('id') in id_list):
            print('Erro duplicated in sent')
            data_verificated['error'] = True
            data_verificated['errors'].append(
                {
                    'id': data_input.get('id'),
                    'position': idx,
                    'id_duplicated': {
                        'value_id': data_input['id'],
                        'position': idx
                    },
                    'status': 'Document not save'
                }
            )
        else:
            publisher_pubsub(
                project_id=GCP_PROJECT_ID,
                topic_name=TOPIC_NAME,
                data=data_input
            )

        id_list.append(data_input.get('id'))

    print(data_verificated)

    return json.loads(json.dumps(data_verificated).encode('utf-8'))


def test_input_data_json(request):
    """Function main

    Args:
        request: Request by user

    Returns:
        request_response (json): Json with data and code status
    """
    print(f'Request: {request}')

    # CORS
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    # Method OPTIONS
    if request.method == 'OPTIONS':
        headers.update({
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600',
            'Content-Type': 'application/json'
        })
        data_send = {
            'message': 'Send data as an example.',
            'example': example
        }
        return (jsonify(data_send), 204, headers)

    if request.method == 'POST':
        # Read data sent json
        data_request = request.get_json()
        file_name = f"raw/{uuid4()}.json"
        # Save data in raw zone
        gcs_upload_json(bucket_name, file_name, data_request)
        secret_key = None
        try:
            # Decode key
            secret_key = jwt.decode(
                request.headers['Authorization'].split(" ")[1],
                'secret',
                algorithms=['HS256']
            )
        except Exception as erro:
            print(f'ERRO: {erro}')
            data_send = {
                'Error': 'Please, inside the correct secret key.'
            }
            return (jsonify(data_send), 401, headers)
        # Test secret key
        if secret_key.get('key') == SECRET_KEY:
            # Verification data
            data_verificated = verification_data(data_request)
            if data_verificated['error']:
                data_verificated['help'] = {
                    'message': 'Send data as an example.',
                    'example': example
                }
                # If not completed all, return 207
                return (jsonify(data_verificated), 207, headers)
            else:
                # If completed all
                return (jsonify(data_verificated), 200, headers)
        else:
            data_send = {
                'Error': 'Please, inside the correct secret key.'
            }
            return (jsonify(data_send), 401, headers)

    data_send = {'Error': 'Method not exist, please try POST method'}
    return (jsonify(data_send), 405, headers)
