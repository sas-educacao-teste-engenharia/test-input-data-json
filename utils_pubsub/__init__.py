from google.cloud import pubsub_v1
import json


def callback(message_future):
    if message_future.exception(timeout=30):
        print(
            'Publishing message on topic threw an Exception {}.'.format(
                message_future.exception()
            )
        )
    else:
        print(message_future.result())


def publisher_pubsub(project_id, topic_name, data):
    """Function to send message Pub/Sub

    Args:
        project_id (string)
        topic_name (string)
        data (dict): Data to send.
    """
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    print('Published message IDs:')
    data = json.dumps(data)
    data = data.encode('utf-8')
    message_future = publisher.publish(topic_path, data=data)
    message_future.add_done_callback(callback)
